# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'form.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(400, 160)
        MainWindow.setMaximumSize(QSize(1280, 16777215))
        self.start_stop_detection = QAction(MainWindow)
        self.start_stop_detection.setObjectName(u"start_stop_detection")
        self.exit_program = QAction(MainWindow)
        self.exit_program.setObjectName(u"exit_program")
        self.action_7 = QAction(MainWindow)
        self.action_7.setObjectName(u"action_7")
        self.action_8 = QAction(MainWindow)
        self.action_8.setObjectName(u"action_8")
        self.action_9 = QAction(MainWindow)
        self.action_9.setObjectName(u"action_9")
        self.action_10 = QAction(MainWindow)
        self.action_10.setObjectName(u"action_10")
        self.action_11 = QAction(MainWindow)
        self.action_11.setObjectName(u"action_11")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.gridLayout = QGridLayout(self.centralwidget)
        self.gridLayout.setSpacing(0)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.statusbar = QLabel(self.centralwidget)
        self.statusbar.setObjectName(u"statusbar")
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.statusbar.sizePolicy().hasHeightForWidth())
        self.statusbar.setSizePolicy(sizePolicy)

        self.gridLayout.addWidget(self.statusbar, 1, 0, 1, 1)

        self.outputbar = QLabel(self.centralwidget)
        self.outputbar.setObjectName(u"outputbar")
        self.outputbar.setMaximumSize(QSize(200, 16))

        self.gridLayout.addWidget(self.outputbar, 1, 1, 1, 1)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.img_1 = QLabel(self.centralwidget)
        self.img_1.setObjectName(u"img_1")
        self.img_1.setEnabled(True)
        self.img_1.setMinimumSize(QSize(100, 100))
        self.img_1.setScaledContents(True)
        self.img_1.setAlignment(Qt.AlignCenter)

        self.horizontalLayout.addWidget(self.img_1)

        self.img_2 = QLabel(self.centralwidget)
        self.img_2.setObjectName(u"img_2")
        self.img_2.setMinimumSize(QSize(100, 100))
        self.img_2.setScaledContents(True)
        self.img_2.setAlignment(Qt.AlignCenter)

        self.horizontalLayout.addWidget(self.img_2)

        self.img_3 = QLabel(self.centralwidget)
        self.img_3.setObjectName(u"img_3")
        self.img_3.setMinimumSize(QSize(100, 100))
        self.img_3.setScaledContents(True)
        self.img_3.setAlignment(Qt.AlignCenter)

        self.horizontalLayout.addWidget(self.img_3)

        self.img_4 = QLabel(self.centralwidget)
        self.img_4.setObjectName(u"img_4")
        self.img_4.setMinimumSize(QSize(0, 100))
        self.img_4.setStyleSheet(u"")
        self.img_4.setScaledContents(True)
        self.img_4.setAlignment(Qt.AlignCenter)
        self.img_4.setWordWrap(False)

        self.horizontalLayout.addWidget(self.img_4)


        self.gridLayout.addLayout(self.horizontalLayout, 0, 0, 1, 2)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 400, 28))
        self.menu = QMenu(self.menubar)
        self.menu.setObjectName(u"menu")
        font = QFont()
        font.setUnderline(False)
        self.menu.setFont(font)
        MainWindow.setMenuBar(self.menubar)

        self.menubar.addAction(self.menu.menuAction())
        self.menu.addAction(self.start_stop_detection)
        self.menu.addSeparator()
        self.menu.addAction(self.exit_program)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"RoadVisor", None))
        self.start_stop_detection.setText(QCoreApplication.translate("MainWindow", u"\u0421\u0442\u0430\u0440\u0442/\u0441\u0442\u043e\u043f", None))
        self.exit_program.setText(QCoreApplication.translate("MainWindow", u"\u0412\u044b\u0445\u043e\u0434", None))
        self.action_7.setText(QCoreApplication.translate("MainWindow", u"\u041d\u0430\u0441\u0442\u0440\u043e\u0438\u0442\u044c \u0441\u043f\u0438\u0441\u043e\u043a \u0437\u043d\u0430\u043a\u043e\u0432", None))
        self.action_8.setText(QCoreApplication.translate("MainWindow", u"\u0412\u043a\u043b\u044e\u0447\u0438\u0442\u044c \u0437\u0432\u0443\u043a\u0438", None))
        self.action_9.setText(QCoreApplication.translate("MainWindow", u"\u0412\u043a\u043b\u044e\u0447\u0438\u0442\u044c \u043e\u0437\u0432\u0443\u043a\u0447\u0443", None))
        self.action_10.setText(QCoreApplication.translate("MainWindow", u"\u041f\u0440\u0438\u0431\u0430\u0432\u0438\u0442\u044c", None))
        self.action_11.setText(QCoreApplication.translate("MainWindow", u"\u0423\u0431\u0430\u0432\u0438\u0442\u044c", None))
        self.statusbar.setText(QCoreApplication.translate("MainWindow", u"<p style=\"color:green;\">\u0413\u043e\u0442\u043e\u0432\u043e</p>", None))
        self.outputbar.setText("")
        self.img_1.setText("")
        self.img_2.setText("")
        self.img_3.setText("")
        self.img_4.setText("")
        self.menu.setTitle(QCoreApplication.translate("MainWindow", u"\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u044f", None))
    # retranslateUi

