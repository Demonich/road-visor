# This Python file uses the following encoding: utf-8
import os
import sys
# import re
import time
from threading import Thread
# from pathlib import Path
from playsound import playsound
# from pydub import AudioSegment
# from pydub.playback import play

# from PySide6.QtUiTools import QUiLoader
from PySide2.QtWidgets import QApplication, QMainWindow
# from PySide6.QtGui import QAction
from PySide2.QtCore import QProcess
from PySide2.QtGui import QPixmap
os.system("pyside2-uic form.ui > ui_form.py")
from ui_form import Ui_MainWindow


class MainWindow(QMainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.text = self.ui.outputbar
        self.detect = None
        self.first_start = True
        self.detection_output = ""
        self.label_list = ['speed 30', 'speed 40', 'speed 50', 'speed 60', 'speed 70', 'speed 80', 'speed 90', 'dangerous turn left', 'dangerous turn right', 'no turn left', 'no turn right', 'crosswalk', 'give way', 'main road', 'movement prohibited', 'no entry', 'no stop', 'overtaking prohibited', 'speed control hump', 'watch the kids', 'stop']
        self.label_current_timer = {a: 0 for a in self.label_list}
        self.label_detected_times = {a: 0 for a in self.label_list}
        self.label_last_time = {a: time.time() for a in self.label_list}
        self.label_present_on_form = {a: 0 for a in self.label_list}
        # self.ui.statusbar.showMessage("Вас приветствует программа RoadVisor.")
        # нажимаем на пункт меню "старт/стоп" который запускает или останавливает модуль
        self.ui.start_stop_detection.triggered.connect(self.start_stop_trigger)
        # нажимаем на пункт меню "выход"
        self.ui.exit_program.triggered.connect(self.exit)

        # self.ui.img_4.setText('<img src="images/cell phone.png"/>')
        # self.ui.img_4.setPixmap(QPixmap("cell phone.png"))
        # a = self.ui.img_4.text()
        # text = a.replace('<img src="images/','').replace('.png"/>','')
        # print (text)

    def get_ui_img_array(self):
        return [None, self.ui.img_1, self.ui.img_2, self.ui.img_3, self.ui.img_4]

    def message(self, s):
        self.text.setText(s)
        self.detection_output = s
#        print (self.detection_output)
#        if self.detection_output.find('cell phone') != -1 :
#            self.ui.statusbar.setText("Телефон обнаружен, поздравляю")
        for i in self.label_list:
            # if i == self.detection_output.find(i) != -1:
            # Когда детектор обнаруживает метку из списка
            if self.label_last_time[i] != 0:
                self.label_current_timer[i] = round((time.time() - self.label_last_time[i]), 2)
            if self.detection_output.find(i) != -1:
                for j in range(1, 5):
                    cell_text = self.get_ui_img_array()[j].text()
                    cell_name = cell_text.replace('<img src="resources/images/', '').replace('.png"/>', '')
                    if cell_name == i and self.label_present_on_form[i] != 1:
                        self.label_present_on_form[i] = 1
                self.label_detected_times[i] += 1
                if self.label_detected_times[i] > 9 and self.label_present_on_form[i] == 0:
                    if self.label_last_time[i] == 0:
                        self.label_last_time[i] = time.time()
                    if self.ui.img_4.text() == "":
                        self.ui.img_4.setText('<img src="resources/images/' + i + '.png"/>')
                        self.label_present_on_form[i] = 1
                        self.label_last_time[i] = time.time()
                        thread = Thread(target=self.play_sound, args=[i])
                        thread.start()
                        # play(AudioSegment.from_wav("audio/" + i + ".wav"))
                if self.label_detected_times[i] > 9 and self.label_present_on_form[i] == 1:
                    self.label_last_time[i] = time.time()
            if self.label_current_timer[i] > 5:
                self.label_present_on_form[i] = 0
                for l in range(1, 5):
                    cell_text = self.get_ui_img_array()[l].text()
                    cell_name = cell_text.replace('<img src="resources/images/', '').replace('.png"/>', '')
                    if cell_name == i:
                        self.get_ui_img_array()[l].clear()
                self.label_detected_times[i] = 0
                self.label_current_timer[i] = 0
                self.label_last_time[i] = 0
        for k in range (1,4):
            cell_text = self.get_ui_img_array()[k+1].text()
            if self.get_ui_img_array()[k].text() == "":
                cell_name = cell_text.replace('<img src="resources/images/', '').replace('.png"/>', '')
                self.get_ui_img_array()[k].setText(cell_text)
                self.get_ui_img_array()[k+1].clear()

    def play_sound(self, file_name):
        playsound("resources/audio/" + file_name + ".wav")
        #play(AudioSegment.from_wav("audio/" + file_name + ".wav"))

    # определение параметров запускаемого модуля (субпроцесса detect.py)
    def set_process_properties(self):
        self.detect.setProgram("python3")
        self.detect.setArguments(['yolov5/detect.py', '--weights', 'resources/models/visor.pt', '--source', '0', '--nosave', '--conf-thres', '0.5'])

        # return ("python3", ['dummy_script.py'])
        # return ("gnome-calculator")
        # return ("python3", ['detect.py --source 0 --nosave'])

    # функция запускает модуль
    def start_stop_trigger(self):
        # если модуль не запущен, запускаем
        if self.detect is None:
            self.detect = QProcess()
            self.detect.readyReadStandardOutput.connect(self.handle_stdout)
            self.detect.readyReadStandardError.connect(self.handle_stderr)
            self.detect.stateChanged.connect(self.handle_state)
            self.detect.finished.connect(self.process_finished)
            self.set_process_properties()
            self.detect.start()
            # self.detect.start("python3", ['dummy_script.py'])
            if self.detect.state == QProcess.NotRunning:
                self.ui.statusbar.setText('<p style="color:red;">Ошибка</p>')
                self.detect = None
        # а если всё таки запущен, то убиваем
        else:
            self.detect.kill()

    def handle_stderr(self):
        data = self.detect.readAllStandardError()
        stderr = bytes(data).decode("utf8")
        self.message(stderr)

    def handle_stdout(self):
        data = self.detect.readAllStandardOutput()
        stdout = bytes(data).decode("utf8")
        self.message(stdout)

    def handle_state(self, state):
        states = {
            QProcess.NotRunning: 'Не запущено',
            QProcess.Starting: 'Запускается',
            QProcess.Running: 'Запущено',
        }
        state_name = states[state]
        self.ui.statusbar.setText(f"{state_name}")

    def process_finished(self):
        self.ui.statusbar.setText('<p style="color:green;">Завершено</p>')
        self.detect = None
        self.text.clear()
        self.ui.img_1.clear()
        self.ui.img_2.clear()
        self.ui.img_3.clear()
        self.ui.img_4.clear()

#    def process_finished(self):
#            self.ui.statusbar.setText("Распознавание остановлено.")
#            self.detect = None

    # функция делает выход отседа
    def exit(self):
        sys.exit(app.exec_())


if __name__ == '__main__':
    app = QApplication(sys.argv)

    window = MainWindow()
    window.show()

    sys.exit(app.exec_())
