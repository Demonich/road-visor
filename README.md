# <div align="center">Road Visor</div>

<div align="center">
Система распознавания дорожных знаков на основе методов машинного обучения
</div>

<div align="center">
<img src="https://gitlab.com/Demonich/road-visor/-/raw/main/resources/icons/hicolor/road-visor.png"/>
</div>

<div align="center">
Данный проект представляет собой компьютерное приложение, на главной форме которого отображаются изображения дорожных знаков, которые попадают в поле зрения камеры видеонаблюдения. За распознавание отвечает <a href="https://github.com/ultralytics/yolov5">YOLOv5</a>.
</div>

## <div align="center">Системные требования</div>

- Операционная система на базе Linux (Например [**Ubuntu**](https://ubuntu.com/) версии 18.04 и выше)
- [**Python>=3.7.0**](https://www.python.org/)
- Видеокарта NVIDIA 
  - с поддержой CUDA
  - *вычислительными возможностями* версии >= 3.7 (проверить соответстует ли ваше устройство можно [на сайте NVIDIA](https://developer.nvidia.com/cuda-gpus#compute))
  - установленные драйвера
- [**CUDA**](https://developer.nvidia.com/cuda-toolkit/)
- [**cuDNN**](https://developer.nvidia.com/rdp/form/cudnn-download-survey)

> ###### ☝️ Примечание
> Приложение разрабатывалось для операционных систем, основанных на Linux, однако в теории возможен запуск на Windows.

## <div align="center">Подготовка</div>

<details>
<summary>Установка Python</summary>

Первым делом нам необходимо определить пакетный менеджер своего дистрибутива.

<details>
<summary>Debian, Ubuntu и основанные на них дистрибутивы — apt</summary>

Введите следующие команды в терминале:

```bash
sudo apt update # обновление списков пакетов
sudo apt install python3 -y # установка Python3 и пакета для загрузки модулей Python
```

</details>

<details> 
<summary>CentOS, Red Hat, Fedora — rpm</summary>

Введите следующие команды в терминале:

```bash
sudo yum install python3 python3-pip -y # установка Python3 и пакета для загрузки модулей Python
```

Для Fedora лучше использовать следующую команду:

```bash
sudo dnf install python3 python3-pip -y # установка Python3 и пакета для загрузки модулей Python
```

</details>

<details>
<summary>ARCH Linux — pacman</summary>

Введите следующие команды в терминале:

```bash
sudo pacman -Sy python3 python3-pip # установка Python3 и пакета для загрузки модулей Python
```

</details>

<details>
<summary>OpenSUSE и SUSE Linux — zypper</summary>

Введите следующие команды в терминале:

```bash
sudo zypper install python3 python3-pip # установка Python3 и пакета для загрузки модулей Python
```

</details>

</details>

<details>
<summary>Установка CUDA</summary>

> ###### ⚠️ ️Внимание!
> Данная инструкция приведена как пример и не является правилом. Более актуальные инструкции для вашего дистрибутива смотрите на официальном сайте или в интернете.

Перед началом установите пакет Wget. Ниже приведены команды для различных пакетных менеджеров

```bash
sudo apt install wget -y # для пакетного менеджера apt
sudo yum install wget -y # для пакетного менеджера rpm
sudo dnf install wget -y # для пакетного менеджера dnf
sudo pacman -Sy wget # для пакетного менеджера pacman
sudo zypper install wget # для пакетного менеджера zypper
```

Теперь можно приступать к установке CUDA:

```bash
wget http://developer.download.nvidia.com/compute/cuda/10.2/Prod/local_installers/cuda_10.2.89_440.33.01_linux.run
sudo sh cuda_10.2.89_440.33.01_linux.run # устанавливайте только CUDA, не устанавливайте драйвер при наличии
sudo ln -s /usr/lib/cuda-10.2 /usr/local/cuda
echo 'export PATH="/usr/local/cuda/bin:$PATH"' >> ~/.bash_profile
echo 'export LD_LIBRARY_PATH="/usr/local/cuda/lib64:$LD_LIBRARY_PATH"' >> ~/.bash_profile
```

</details>

<details>
<summary>Установка cuDNN</summary>

> ###### ⚠️ ️Внимание!
> Данная инструкция приведена как пример и не является правилом. Более актуальные инструкции для вашего дистрибутива смотрите на официальном сайте или в интернете.

1. Скачайте архив с cuDNN [на сайте](https://developer.nvidia.com/rdp/form/cudnn-download-survey) (нужна авторизация).

> ###### ❗ Важно
> Архив должен соответствовать установленной версии CUDA.

2. Для удобства скопируйте архив в домашний каталог (*/home/имя-пользователя/*)

3. Выполните по очереди команды в терминале:

```bash
tar -xzvf cudnn-10.1-linux-x64-v7.6.5.32.tgz
sudo cp cuda/include/cudnn.h /usr/local/cuda/include
sudo cp cuda/lib64/libcudnn* /usr/local/cuda/lib64
sudo chmod a+r /usr/local/cuda/include/curm /usr/local/cuda/targets/x86_64-linux/lib/libcudnn.so.7 /usr/local/cuda/targets/x86_64-linux/lib/libcudnn.so
sudo ln -s libcudnn.so.7.6.5 /usr/local/cuda/targets/x86_64-linux/lib/libcudnn.so.7
sudo ln -s libcudnn.so.7 /usr/local/cuda/targets/x86_64-linux/lib/libcudnn.so
sudo ldconfigdnn.h /usr/local/cuda/lib64/libcudnn*
```

</details>

## <div align="center">Установка</div>

Клонируйте репозиторий и установите зависимости из [requirements.txt](https://gitlab.com/Demonich/road-visor/-/blob/main/requirements.txt):

```bash
git clone https://gitlab.com/Demonich/road-visor.git # клонирование
cd road-visor # переход в каталог
pip3 install -r requirements.txt  # установка
```

Не закрывая терминал установите внутри каталога [YOLOv5](https://github.com/ultralytics/yolov5) со всеми зависимостями:

```bash
git clone https://github.com/ultralytics/yolov5  # клонирование
cd yolov5 # переход в каталог
pip3 install -r requirements.txt # установка
```